package ru.specialist.spring.dao;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.specialist.spring.config.DataConfig;
import ru.specialist.spring.dao.repository.PostRepository;
import ru.specialist.spring.dao.repository.UserRepository;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.entity.User;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DataConfig.class)
@Sql(scripts = "classpath:schema.sql",
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
public class UserRepositoryTest {


    private final UserRepository userRepository;

    @Autowired
    public UserRepositoryTest(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Test
    void create(){
        User user = new User();
        user.setUsername("Fillipok");
        user.setFirstName("Fillip");
        user.setLastName("Voronin");
        user.setPassword("fil1234");
        user.setActive(true);

        userRepository.save(user);

        assertEquals("Fillipok", userRepository.findById(5L).get().getUsername());
    }


    @Test
    void delete() {
        userRepository.deleteById(3L);
        assertEquals(3, userRepository.count());
    }

    @Test
    void findByUsername(){
        User user = userRepository.findByUsername("ZenTiago").get();
        assertEquals("Victor", user.getFirstName());
    }




}
